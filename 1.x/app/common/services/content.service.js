(function () {
	"use strict";

	angular
		.module("common")
		.service("commonContentService", commonContentService);

	
	function commonContentService($http) {

		//API
		return {
			loadBY: loadBY,
			loadRU: loadRU
		};


		function loadBY() {
			return $http.get("common/content.by.json");
		};

		function loadRU() {
			return $http.get("common/content.ru.json");
		};
	};
})();