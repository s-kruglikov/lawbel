(function () {
	"use strict";

	angular.module("app", [
		"common",
		"contacts",
		"duScroll",
		"footer",
		"services",
		"sidebar",
		"welcome"]);
})();