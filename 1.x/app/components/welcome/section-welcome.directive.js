(function() {
    "use strict";

    angular
        .module("welcome")
        .directive("sectionWelcome", sectionWelcome);

    function sectionWelcome() {
        return {
            restrict: "EA",
            templateUrl: "components/welcome/section-welcome.html"
        }
    };
})();