(function() {
    "use strict";

    angular
        .module("services")
        .directive("sectionServices", sectionServices);

    function sectionServices() {
        return {
            restrict: "EA",
            templateUrl: "components/services/section-services.html"
        }
    };
})();