(function() {
    "use strict";

    angular
        .module("footer")
        .directive("sectionFooter", sectionFooter);

    function sectionFooter() {
        return {
            restrict: "EA",
            templateUrl: "components/footer/section-footer.html"
        }
    };
})();