(function () {
	"use strict";

	angular.module("contacts")
		.controller("Contacts", Contacts);

	function Contacts($scope) {
		
		$scope.hintContent = null;
		$scope.balloonContent = null;
		$scope.ymap = null;

		$scope.$on("contentLoaded", function(event, args){
			$scope.hintContent = args.hintContent;
			$scope.balloonContent = args.balloonContent;

			/* Yandex Maps */
			
			ymaps.ready(initYandexMaps);
		});

		function initYandexMaps() {
			if($scope.ymap) {
				$scope.ymap.destroy();
			}

			$scope.ymap = new ymaps.Map("map", {
				center: [53.90494657, 30.34381450],
				zoom: 16
			});

			var myPlacemark = new ymaps.Placemark([53.90494657, 30.34381450], {
				hintContent: $scope.hintContent,
				balloonContent: $scope.balloonContent
			});

			$scope.ymap.geoObjects.add(myPlacemark);
		};
	};
})();