(function() {
    "use strict";

    angular
        .module("contacts")
        .directive("sectionContacts", sectionContacts);

    function sectionContacts() {
        return {
            restrict: "EA",
			templateUrl: "components/contacts/section-contacts.html",
			scope: true
        }
    };
})();