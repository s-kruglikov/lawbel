(function () {
	"use strict";

	angular
		.module("sidebar")
		.directive("sectionSidebar", sectionSidebar);

	function sectionSidebar() {
		return {
			restrict: "EA",
			templateUrl: "components/sidebar/section-sidebar.html"
		}
	};
})();