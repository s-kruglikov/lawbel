(function () {
	"use strict;"

	angular.module("app").controller("Index", Index);

	//controller
	function Index($scope, $rootScope, commonContentService) {

		$scope.content = null;
		$scope.currentLang = null;

		$scope.loadRU = function(){
			commonContentService.loadRU().then(function (response) {
				$scope.content = response.data;
				$scope.currentLang = "RU";
				$rootScope.$broadcast("contentLoaded", $scope.content);
			});	
		};

		$scope.loadBY = function(){
			commonContentService.loadBY().then(function (response) {
				$scope.content = response.data;
				$scope.currentLang = "BY";
				$rootScope.$broadcast("contentLoaded", $scope.content);
			});
		};

		$scope.isActiveLang = function(langShortcut){
			if(langShortcut == $scope.currentLang)
				return "active-lang";
		};

		$scope.init = function(){
			$scope.loadRU();
		}
	};
})();