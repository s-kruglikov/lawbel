// Various helper modules
var gulp = require("gulp");
var clean = require('gulp-clean');
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');
var ngAnnotate = require('gulp-ng-annotate');
var plug = require("gulp-load-plugins")();
var uglify = require('gulp-uglify');

var merge = require('merge-stream');
var runSequence = require('run-sequence');

/* Paths constants */
var commonPaths = ['',
	'/common',
	'/common/services'];

var componentsPaths = ['',
			 '/components/contacts',
			 '/components/footer',
			 '/components/services',
			 '/components/sidebar',
			 '/components/welcome'];

var srcRoot = 'app';
var relRoot = 'release';

/* Gulp tasks */
gulp.task('webserver', function () {
	return gulp
		.src(srcRoot)										// root
		//.src(relRoot)
		.pipe(plug.webserver({
			livereload: true,							// livereload
			directoryListing: true,
			open: "http://localhost:8000/index.html"	// index.html
		}));
});

/* #region Tasks */

gulp.task('cleanrel', function () {
	return gulp.src(relRoot, { read: false })
		.pipe(clean());
});

gulp.task('cprel', function () {
	return gulp.src(srcRoot + '/**')
		.pipe(gulp.dest(relRoot));
});

gulp.task('mincss', function () {
	return gulp.src('app/assets/styles/*.css')
		.pipe(cleanCSS({ compatibility: 'ie8', keepClosingSlash: true }))
		.pipe(gulp.dest('release/assets/styles'));
});

gulp.task('minhtml', function () {
	var fileext = '/*.html';
	var tasks = componentsPaths.map(function (path) {
		return gulp.src(srcRoot + path + fileext)
			.pipe(htmlmin({ collapseWhitespace: true, }))
			.pipe(gulp.dest(relRoot + path));
	});

	return merge(tasks);
});

gulp.task('minjs', function () {
	var fileext = '/*.js';
	var jsPaths = commonPaths.concat(componentsPaths);

	var tasks = jsPaths.map(function (path) {
		return gulp.src(srcRoot + path + fileext)
			.pipe(ngAnnotate())
			.pipe(uglify().on('error', function (e) {
				console.log(e);
			}))
			.pipe(gulp.dest(relRoot + path))
	});

	return merge(tasks);
});

gulp.task('release', function(callback) {
	runSequence('cleanrel',	'cprel',			//sequential
				['mincss', 'minhtml', 'minjs'], //parallel
				function() {});					//callback
});

// The default task is 'webserver'
gulp.task("default", ["webserver"]);

/* #endregion Tasks */