# Summary #
Lawbel.by source code repository. Attorney's landing page site build with Angular.

# Set Up #
## Setup Environment ##

* install node.js from https://nodejs.org/en/

* install git from https://git-scm.com/ See options in git-option.png

* install bower from command line:

```
#!
npm install -g bower
```

*  install gulp from command line:

```
#!
npm install -g gulp
```

* run project from project folder:

```
#!
npm start
```

# Release #
* execute gulp release task:

```
#!
gulp release
```

# Contributors #
Stanislav Kruglikov [Stanislav_Kruglikov@hotmail.com]